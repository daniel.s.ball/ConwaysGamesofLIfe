#include <Arduino.h>
#include <Wire.h>
#include "Adafruit_SSD1306.h"
#include "SPI.h"
#include "Adafruit_FRAM_SPI.h"

enum class OPCODE: uint8_t{
  WREN  =  0x06,
  WRDI  =   0x04,
  WRITE =  0x02,
  READ  =  0x03,
  SEC_WRITE = 0x12,
  SEC_READ = 0x13,
  WRSR  =  0x01,
  RDSR  =  0x05,
  STORE = 0x08,
  RECALL = 0x09,
  WRNUR = 0xC2,
  RDNUR = 0xC3,
  HIBERNATE = 0xB9,
};

void print_welcome();
void reset_display();
void init_display();
void init_spi();

const uint8_t address = 0x3C;
const uint8_t width = 128;
const uint8_t height = 32;
Adafruit_SSD1306 display(width, height, &Wire, -1, 100000UL, 100000UL);



const uint8_t mosi = D10;
const uint8_t miso = D9;
const uint8_t sck = D8;
const uint8_t cs_ = D0;
const uint8_t hold_ = D1;
Adafruit_FRAM_SPI fram = Adafruit_FRAM_SPI(sck, miso, mosi, cs_);


void init_spi(){
  pinMode(cs_, OUTPUT);
  pinMode(hold_, OUTPUT);
  pinMode(miso, INPUT);
  pinMode(sck, OUTPUT);
  pinMode(hold_, OUTPUT);
}

void init_display(){
  Wire.begin();
  
  display.begin(SSD1306_SWITCHCAPVCC, address, true, true);
  print_welcome();
}

void reset_display(){
  display.clearDisplay();
  display.setCursor(0,0);
}

void print_welcome(){
  reset_display();
  display.print("Ready...");
  display.display();
  delay(1000);
}

void init_fram(){
  reset_display();

  if(fram.begin()){
    display.print("Found SPI FRAM");
  }else{
    display.print("NO FRAM FOUND :(");
  }

  display.display();
}


void setup() {
  init_display();
  init_spi();
}

uint8_t write_buffer[10];
uint8_t read_buffer[10];


void loop() {
}