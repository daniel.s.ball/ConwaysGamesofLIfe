#include "EERAM.h"
#include "SPI.h"

enum StatusBits{
	ASE_ = 6,
	SWM_ = 4,
	BP1 = 3,
	BP0 = 2,
	WEL = 1,
	BSY = 0	
};

// 6.4: Read STATUS Register (RDSR)
/**
 * assert CS_
 * send opcode
 * return value is in RX register
 * @return
 */
uint8_t EERAM::readStatusRegister() {

    tx_buf_intern[0] = OPCODES::READ_STATUS;
    spi->xfer(tx_buf_intern, rx_buf_intern, 2); // 2 bytes: command, and clock in rx
	return rx_buf_intern[1];	// received value will be in second position
}

void EERAM::writeStatusRegister(){
	
}


EERAM::EERAM(SPI* spiHandle) : spi(spiHandle){}
EERAM::~EERAM(){}

		


