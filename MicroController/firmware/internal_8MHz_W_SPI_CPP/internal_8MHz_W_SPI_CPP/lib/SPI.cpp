#include "SPI.h"


SPI* SPI::instance = nullptr;

void SPI::write(uint8_t* tx_buf, uint32_t length){
	assert(tx_buf != nullptr, __FILE__, __LINE__);
	assert(instance != nullptr, __FILE__, __LINE__);
	CSToken session;
	io_write(io, tx_buf, 12);
}

void SPI::read(uint8_t* rx_buf, uint32_t length){
	assert(rx_buf != nullptr, __FILE__, __LINE__);
	assert(instance != nullptr, __FILE__, __LINE__);
	CSToken session;
	io_read(io, rx_buf, length);
}

/**
 * Blocking synchronous transfer
 * @param tx_buf transmit buffer
 * @param rx_buf receive buffer
 * @param size number of bytes to transceive
 */
void SPI::xfer(uint8_t* tx_buf, uint8_t* rx_buf, uint32_t size){
	assert(rx_buf != nullptr, __FILE__, __LINE__);
	assert(tx_buf != nullptr, __FILE__, __LINE__);
	assert(instance != nullptr, __FILE__, __LINE__);
	CSToken session;
	spi_m_sync_transfer(&SPI_0, p_xfer);
}

void inline SPI::acquire(){
	gpio_set_pin_level(CS_, 0);
}

void inline SPI::release(){
	gpio_set_pin_level(CS_, 1);
}

SPI* SPI::getInstance(){
	if (instance == nullptr){
		instance = new SPI();
	}
	return instance;
}

void SPI::setMode(SPI::MODE mode) {
    spi_m_sync_set_mode(&SPI_0, static_cast<spi_transfer_mode>(mode));
}

SPI::SPI(){
	spi_m_sync_enable(&SPI_0);
	spi_m_sync_get_io_descriptor(&SPI_0, &io); // create io descriptor for read/write
}

SPI::~SPI(){
	spi_m_sync_disable(&SPI_0);
}

