#ifndef __EERAM_H__
#define __EERAM_H__

#include "util.h"
#include "hal_spi_m_sync.h"
#include "sam.h"
#include <atmel_start.h>
#include "SPI.h"


/**
 * SEE TABLE 4-1: INSTRUCTION SET
 */
namespace OPCODES{
    const uint8_t SET_WRITE_ENABLE(0x06);
    const uint8_t RESET_WRITE_ENABLE(0x06);
    const uint8_t WRITE(0x06);
    const uint8_t READ(0x06);
    const uint8_t SECURE_WRITE(0x06);  // validated write with CRC16-CCITT with x^{16,12,5,1}
    const uint8_t SECURE_READ(0x06);   // validated read with CRC16-CCITT with x^{16,12,5,1}
    const uint8_t WRITE_STATUS(0x06);
    const uint8_t READ_STATUS(0x06);
    const uint8_t STORE(0x06);
    const uint8_t RECALL(0x06);
    const uint8_t WRITE_USER_SPACE(0x06);
    const uint8_t READ_USER_SPACE(0x06);
    const uint8_t HIBERNATE(0x06);
};


class EERAM
{
    //variables
    public:
    protected:
    private:
        SPI* spi;
        uint8_t tx_buf_intern[32] = {0};	// used for control sequences
        uint8_t rx_buf_intern[32] = {0};
        spi_xfer internal_xfer;
        spi_xfer xfer_desc;

    //functions
    public:
        EERAM(SPI* spiHandle);
        ~EERAM();

        uint8_t readStatusRegister();
        void writeStatusRegister();
        uint8_t readByte(uint32_t address);
        void writeByte(uint32_t address);
        void readBytes(uint32_t address, uint32_t length, uint8_t* readBuffer);
        void writeBytes(uint32_t address, uint32_t length, uint8_t* writeBuffer);

    protected:
    private:
        EERAM( const EERAM &c );
		EERAM& operator=( const EERAM &c );
}; //EERAM

#endif //__EERAM_H__
