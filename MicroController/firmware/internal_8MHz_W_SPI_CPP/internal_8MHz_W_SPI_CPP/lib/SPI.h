#ifndef SPI_H
#define SPI_H

#include "hal_spi_m_sync.h"
#include "sam.h"
#include <atmel_start.h>
#include <mutex>

class SPI{
	// variables
	public:	
	enum class EXCEPT{
		NULL_POINTER,
		NONE
	};

	enum class MODE{_0,_1,_2,_3};

	private:
	static SPI* instance;

	// functions
	public:
	void write(uint8_t* tx_buf, uint32_t length);
	void read(uint8_t* rx_buf, uint32_t length);
	void xfer(uint8_t* tx_buf, uint8_t* rx_buf, uint32_t size);

    void setMode(SPI::MODE mode);

	private:
	io_descriptor* io;
	spi_xfer* p_xfer;
	// Allow only a single instance.
	SPI(SPI &other) = delete;
	SPI();
	void operator=(const SPI& _) = delete;	

	void inline acquire();
	void inline release();
	public:
	static SPI* getInstance();


	~SPI();
	
	class CSToken{
		private:
		CSToken(const CSToken&) = delete;
		CSToken& operator=(const CSToken&) = delete;
		
		public:
		CSToken(){
			gpio_set_pin_level(CS_, 0); // select
		}

		~CSToken(){
			gpio_set_pin_level(CS_, 1); // deselect
		}
	};
};

#endif //SPI_H
