#include "sam.h"
#include <atmel_start.h>
#include "color.h"
#include "SPI.h"


Color color;


void inline acquire(){
	gpio_set_pin_level(CS_, 0);
}

void inline release(){
	gpio_set_pin_level(CS_, 1);
}

void spi_setup(io_descriptor* io){
	spi_m_sync_enable(&SPI_0);	// enable spi sync
	spi_m_sync_get_io_descriptor(&SPI_0, &io); // create io descriptor for read/write
	spi_m_sync_set_mode(&SPI_0, SPI_MODE_0);
}

void helloSequence(){
	color.set_color(0b00100000);
	delay_ms(500);
	color.set_color(0b00100100);
	delay_ms(500);
	color.set_color(0b00000001);
	delay_ms(500);
	color.set_color(0b00000000);
}

int main(void)
{		
	/* Initializes MCU, drivers and middleware */
	// SPI_0: Mode 0
	// MSb first
	// SO latched on falling edge
	
	// TIMER_0, gpio
	atmel_start_init();
	helloSequence();
	
	
	
	//uint8_t tx_buf[16];
	//tx_buf[0] = 0x06; // read status opcode
	//uint8_t rx_buf[16] = {0};
	//spi_xfer p_xfer = { tx_buf ,rx_buf, sizeof(tx_buf)/sizeof(tx_buf[0])};
	//io_descriptor* io = nullptr;
	//spi_setup(io);
	//
	//// read status
	//acquire();
	//int32_t result = spi_m_sync_transfer(&SPI_0, &p_xfer);
	//release();
	//
	//if(result == 0){
		//color.set_color(0b00100000);
		//}else{
		//color.set_color(0b00100100);
	//}

	while (1) {
		helloSequence();
	}
}
