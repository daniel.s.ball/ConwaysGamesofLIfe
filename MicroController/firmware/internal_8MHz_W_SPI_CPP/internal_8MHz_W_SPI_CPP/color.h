#include <cstdint>
#include "atmel_start_pins.h"

#ifndef INCFILE1_H_
#define INCFILE1_H_

class Color{
	const uint8_t RGB_pins[8] = {D8, D7, D6, D5, D4, D3, D2, D1};
	const uint8_t R_pins[3] = {D8, D7, D6};
	const uint8_t G_pins[3] = {D5, D4, D3};
	const uint8_t B_pins[2] = {D2, D1};
	
	public:
	void set_color(const uint8_t color);
};



#endif /* INCFILE1_H_ */