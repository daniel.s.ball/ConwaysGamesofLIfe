/*
 * CFile1.c
 *
 * Created: 3/28/2023 8:31:06 PM
 *  Author: daniel
 */ 
#include "color.h"
#include "hal/include/hal_gpio.h"

void Color::set_color(const uint8_t color)
{
	bool bit_on = false;
	for(uint8_t bit = 0; bit < 7; bit++){
		bit_on = (color & 0b1<<bit) > 0;
		gpio_set_pin_level(RGB_pins[7-bit], bit_on);
	}
}
