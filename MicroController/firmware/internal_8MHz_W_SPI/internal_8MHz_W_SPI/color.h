/*
 * color.h
 *
 * Created: 3/28/2023 8:31:59 PM
 *  Author: daniel
 */ 

#include "hal_gpio.h"


#ifndef INCFILE1_H_
#define INCFILE1_H_

class Color{
	const uint8_t RGB_pins[] = {D8, D7, D6, D5, D4, D3, D2, D1};
	const uint8_t R_pins[] = {D8, D7, D6};
	const uint8_t G_pins[] = {D5, D4, D3};
	const uint8_t B_pins[] = {D2, D1};
	
	void set_color(uint8_t color);
}color;



#endif /* INCFILE1_H_ */