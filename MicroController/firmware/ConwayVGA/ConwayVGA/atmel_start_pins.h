/*
 * Code generated from Atmel Start.
 *
 * This file will be overwritten when reconfiguring your Atmel Start project.
 * Please copy examples or other code you want to keep to a separate file
 * to avoid losing it when reconfiguring.
 */
#ifndef ATMEL_START_PINS_H_INCLUDED
#define ATMEL_START_PINS_H_INCLUDED

#include <hal_gpio.h>

// SAMD21 has 8 pin functions

#define GPIO_PIN_FUNCTION_A 0
#define GPIO_PIN_FUNCTION_B 1
#define GPIO_PIN_FUNCTION_C 2
#define GPIO_PIN_FUNCTION_D 3
#define GPIO_PIN_FUNCTION_E 4
#define GPIO_PIN_FUNCTION_F 5
#define GPIO_PIN_FUNCTION_G 6
#define GPIO_PIN_FUNCTION_H 7

#define CLK_23768_A GPIO(GPIO_PORTA, 0)
#define CLK_23768_B GPIO(GPIO_PORTA, 1)
#define HOLD_ GPIO(GPIO_PORTA, 2)
#define CS_ GPIO(GPIO_PORTA, 3)
#define PA04 GPIO(GPIO_PORTA, 4)
#define PA05 GPIO(GPIO_PORTA, 5)
#define PA06 GPIO(GPIO_PORTA, 6)
#define D3 GPIO(GPIO_PORTA, 7)
#define D4 GPIO(GPIO_PORTA, 8)
#define D5 GPIO(GPIO_PORTA, 9)
#define D1 GPIO(GPIO_PORTA, 10)
#define D2 GPIO(GPIO_PORTA, 11)
#define CLK_251750_A GPIO(GPIO_PORTA, 14)
#define CLK_251750_B GPIO(GPIO_PORTA, 15)
#define D6 GPIO(GPIO_PORTA, 16)
#define D7 GPIO(GPIO_PORTA, 17)
#define D8 GPIO(GPIO_PORTA, 18)
#define HSYNC GPIO(GPIO_PORTA, 19)
#define VSYNC GPIO(GPIO_PORTA, 22)
#define USB_DM GPIO(GPIO_PORTA, 24)
#define USB_DP GPIO(GPIO_PORTA, 25)

#endif // ATMEL_START_PINS_H_INCLUDED
