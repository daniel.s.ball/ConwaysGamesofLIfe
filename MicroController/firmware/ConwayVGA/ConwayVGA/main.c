#include <atmel_start.h>

const uint8_t RGB_pins[] = {D8, D7, D6, D5, D4, D3, D2, D1};
const uint8_t R_pins[] = {D8, D7, D6};
const uint8_t G_pins[] = {D5, D4, D3};
const uint8_t B_pins[] = {D2, D1};

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	for(uint8_t i = 0; i < 8; i++){
		gpio_set_pin_level(RGB_pins[i], true);
	}

	/* Replace with your application code */
	while (1) {
	}
}