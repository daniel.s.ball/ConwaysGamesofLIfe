#rules:
#1) If a cell is alive, it stays alive if it has either 2 or 3 live neighbors
#2) If a cell is dead, it springs to life only if it has 3 live neighbors
from random import randint
import os
from time import sleep

#https://www.tutorialspoint.com/how-to-clear-screen-in-python#:~:text=In%20Python%20sometimes%20we%20have,screen%20by%20pressing%20Control%20%2B%20l%20.
def screen_clear():
   # for mac and linux(here, os.name is 'posix')
   if os.name == 'posix':
      _ = os.system('clear')
   else:
      # for windows platfrom
      _ = os.system('cls')


class World():
    def __init__(self, rows, cols):
        self.rows = rows                                
        self.cols = cols
        self.cells = [[int(randint(0,4) == 0) for j in range(cols)] for i in range(rows)]
        # self.cells = [[0 for j in range(cols)] for i in range(rows)]
        self.adjacencies = [[0 for j in range(cols)] for i in range(rows)]

    def updateAdjacency(self, row, col):
        sum = 0
        for i in range(row-1, row+2):
            for j in range(col-1, col+2):
                sum += self.cells[i%self.rows][j%self.cols] # % for wrapping, world is a torus
        self.adjacencies[row][col] = sum - self.cells[row][col]

    def updateCell(self, row, col):
        if self.cells[row][col]:    # cell is present
            if self.adjacencies[row][col] != 2 and self.adjacencies[row][col] != 3: # over or under population, cell dies
                self.cells[row][col] = 0
        else:                       # cell is not present
            if self.adjacencies[row][col] == 3:         # 2 adjacent cells -> birth
                self.cells[row][col] = 1

    def updateAdjacencies(self):
        for i in range(self.rows):
            for j in range(self.cols):
                self.updateAdjacency(i, j)

    def updateCells(self):
        for i in range(self.rows):
            for j in range(self.cols):
                self.updateCell(i, j)

    def loop(self):
        self.updateAdjacencies()        
        world.print()
        self.updateCells()

    def print(self):
        screen_clear()
        for row in range(self.rows):
            for val in self.cells[row]:
                print('█' if val else ' ', end='')
            print(' ', end='')
            for val in self.adjacencies[row]:
                print(val, end='')
            print()
           

if __name__ == "__main__":
    world = World(20, 100)
    while True:  
        world.loop()
        sleep(.1)