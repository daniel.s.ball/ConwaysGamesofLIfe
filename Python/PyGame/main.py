from world import World
import pygame





if __name__ == "__main__":
    pygame.init()
    viewSize = (100,100)
    
    world = World(3*viewSize[0], 3*viewSize[1])
    screenOriginOffset = (2*viewSize[0], 2*viewSize[1])
    cellSize = (8,8)
    screenSize = (viewSize[0]*cellSize[0], viewSize[1]*cellSize[1])
    clock = pygame.time.Clock()
    screen = pygame.display.set_mode(screenSize)
    screen.fill((255, 255, 255))
    running = True

    ontime = 0
    offtime = 0

    while running:
        # Did the user click the window close button?
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        world.loop()
        for r in range(viewSize[0] ):
            for c in range(viewSize[1]):
                
                if world.cells[r + screenOriginOffset[0]][c + screenOriginOffset[1]]:
                    pygame.draw.rect(screen, 0x000000, pygame.Rect((c*cellSize[1],r*cellSize[0]),cellSize))
                else:
                    pygame.draw.rect(screen, 0xFFFFFF, pygame.Rect((c*cellSize[1],r*cellSize[0]),cellSize))
       
        # Flip the display
        pygame.display.flip()

    # Done! Time to quit.
    pygame.quit()