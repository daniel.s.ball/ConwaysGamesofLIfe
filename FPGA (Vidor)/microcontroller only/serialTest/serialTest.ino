void setup() {
  Serial.begin(9600);
  while (!Serial) {}
}

void loop() {
  while (Serial.available() > 0) {      // this shouldn't be necessary, but I get COM busy if I don't do it?
    Serial.print(char(Serial.read()));
  }
  if(!Serial.available()){    
    Serial.println("Test");
  }
  delay(1000);
}
