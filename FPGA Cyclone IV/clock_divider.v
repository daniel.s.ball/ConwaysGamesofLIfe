// ///////////////////////////////////////
// Author: 	Daniel Ball 				//
// Date: 	Aug 30, 2019				//
// Project:	variable_clock_divider  	//
// Contact: daniel.s.ball@gmail.com 	//
// ///////////////////////////////////////


// output frequency will be input/(limit*2)
// example:
// clock_divider #(50) clock_div_25M_to_500K(clock25MHZ, clock500KHZ);	// 25M to 500K
module clock_divider(
    input input_clock,
    output reg output_clock
    );

	parameter limit;				
	reg [25:0] count = 0;
	
	always@(posedge input_clock)begin
		count = count + 1; 
		if(count == limit) begin
				count <= 0;
				output_clock <= ~output_clock;
		end
	end
endmodule