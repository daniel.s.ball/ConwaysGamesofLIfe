module up_down_counter(
	input clock,
	input reset,
	input updown,
	output reg [2:0] out
);

reg up;

always@(posedge clock) begin
	if(~reset) begin
		if(updown) begin
			if(out == 0)
				up = 1;
			if(out == 7)
				up = 0;
		end
		if(up)
			out = out + 1;
		else
			out = out - 1;
	end
	else begin
		out = 0;
		up = 1;
	end
end

endmodule