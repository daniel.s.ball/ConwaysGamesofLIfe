
// //////////////////////////////////////
// Author: 	Daniel Ball 					//
// Date: 	Aug 30, 2019					//
// Project:	multi_clock	   				//
// Contact: daniel.s.ball@gmail.com 	//
// //////////////////////////////////////
// Usage example for clock_divider.v
module multi_clock(
        input clock50MHZ,
        output reg clock25MHZ,
        output clock500KHZ,
        output clock1KHZ,
        output clock1HZ
    );

    always@(posedge clock50MHZ)
        clock25MHZ = ~clock25MHZ;	// use simple toggle to do a 1/2 division

    clock_divider #(50) cdiv25M_500K(clock25MHZ, clock500KHZ);	// 25M to 500K
    clock_divider #(500) cdiv500K_1K(clock500KHZ, clock1KHZ);	// 500K to 1K
    clock_divider #(1000) cdiv1K_1(clock1KHZ, clock1HZ);			// 1K to 1Hz
endmodule