// The DE0 nano timer is 50MHz.

module VGA(
	/* VGA SPECIFICATION
	VESA 800x600 @ 72 Hz
	50 MHz pixel frequency
	
	/// horizontal ///
	visible - 800px
	front porch - 56 px
	sync pulse - 120 px
	back porch - 64 px
	line total - 1040 px
	
	/// vertical ///
	visible - 600 lines
	front porch - 37 lines
	sync pulse - 6 lines
	back porch - 23 lines
	full frame - 666 lines
	
	*/
	input clock_50,
	output HSYNC,
	output VSYNC,
	output reg [0:2] R,
	output reg [0:2] G,
	output reg [0:1] B
);

hsync_counter up_down_counter(input clock,	input reset,	input updown,	output reg [2:0] out);



endmodule