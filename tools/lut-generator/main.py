import random
import time
from dataclasses import dataclass

import pygame


"""
Rules:
living cells:
1 or 0 neighbors: death
4 to 8 neighbors: death
2 or 3 neighbors: life

How to deal with a lookup table

  A B C
1 1 8 1
2 1 X 1
3 1 8 1


to compute Byte B2, we need the LSB of all of the A column, all of the B column, and the MSB of the C column.

Naively, we can pass all of A1 and C1 to compute the result, but that would make the LUT substantially larger.
Alternatively we could pass a 10 bit value for the middle row, but return only an 8 bit value.

We could operate on 4 bit values, but if the goal is to operate on boundaries,
we would still need to pass information from left and right cells.

With an 8,10,8 format we have a table with 256 * 1024 * 256, or ~64 MB :/

What about a 4, 6, 4 format? 2^4 * 2^6 * 2^4 = 2^(14) = 16,384 entries. More reasonable, but still 16 kb.

A single cell could have its own truth table ...
"""


def count_bits(value: int):
    count = 0
    while value > 0:
        count += value & 0b1
        value = value >> 1
    return count


def compute_next_cell_state(top: int, mid: int, bot: int):
    """
    inputs are 3 bit values representing cells in a 3x3 grid
    returns: 1 if the cell should be alive next cycle, 0 otherwise

    Rules:
    living cells:
    1 or 0 neighbors: death
    4 to 8 neighbors: death
    2 or 3 neighbors: life

    dead cells:
    3 neighbors: birth
    """
    neighbors = count_bits(top) + count_bits(bot) + count_bits(mid & 0b101)
    living = mid & 0b010
    return (not living and (neighbors == 3)) or (living and ((neighbors == 2) or (neighbors == 3)))


class GameOfLife:
    _TILE_SIZE = 8
    _WIDTH = 600
    _HEIGHT = 800

    def __init__(self, length, width, initial_starts = ()):
        self.current_live = set()
        self.next_live = set()
        self.neighbors = set()

    def search_board(self):



    def update_world(self):
        for row in range(1, self.rows):
            for col in range(1, self.col_chunks):
                self.update_cell(row, col)




pygame.init()
live_color = (0, 255, 100)
bg_color = (200, 200, 200)
line_color = (0, 0, 0)

width, height = 800, 800
tile_size = 8
grid_width = width // tile_size
grid_height = height // tile_size
FPS = 60

screen = pygame.display.set_mode((width, height))
clock = pygame.time.Clock()

# world = GameOfLife()
# world.initialize_world()
# world.display_world()
# for i in range(1000):
#     print("\033[%d;%dH" % (0, 0))
#     world.update_world()
#     world.display_world()
#     time.sleep(0.25)


@dataclass
class Coordinate:
    row: int
    col: int

def draw_grid(live_cell_locations):
    for row in range(grid_height):
        pygame.draw.line(screen, line_color, (0, row * tile_size), (width, row * tile_size))

    for col in range(grid_width):
        pygame.draw.line(screen, line_color, (col * tile_size, 0), (col * tile_size, height))

    for coord in live_cell_locations:
        pygame.draw.rect(screen, live_color, (coord.col*tile_size, coord.row*tile_size, tile_size, tile_size))


def main():
    running = True
    while running:
        clock.tick(FPS)


        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            if event.type == pygame.MOUSEBUTTONDOWN:
                x, y = pygame.mouse.get_pos()
                coord = Coordinate(x//tile_size, y//tile_size)
                if coord in live_cells:
                    live_cells.remove(coord)
                else:
                    live_cells.add(Coordinate)



        screen.fill(bg_color)
        draw_grid(live_cells)
        pygame.display.update()

    pygame.quit()

if __name__ == "__main__":
    main()